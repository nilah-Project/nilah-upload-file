if(Meteor.isServer) {
  Meteor.methods({
    uploadFile: function(dataId, dataLength, datasend, dataName) {
      var ret;
      if(dataId == 0) {
        var id = Meteor.uuid();
        Filess = new Meteor.Collection(null);
        Filess.insert({_id: id, data: datasend});
        thisFile = Filess.findOne();
        ret = {
          _id: thisFile._id,
          length: thisFile.data.length
        }
        thisData = thisFile.data;
      }
      else {
        thisFile = Filess.findOne({_id: dataId});
        thisData = thisFile.data;
        for(var j = 0; j < datasend.length; j++) {
          thisData.push(datasend[j]);
        }

        Filess.update({_id: dataId}, {$set: {data: thisData}});
        ret = {
          _id: thisFile._id,
          length: thisData.length
        }
      }
      if(thisData.length == dataLength) {

        //write file begin
        var dirPath = "/home/nilaina/m-ite/up";
        var fs = Npm.require('fs');
        var path = Npm.require('path');
        rootPath = path.resolve('.');
        absolutePath = rootPath.split(path.sep + '.meteor')[0] + '/.upload';
        var filepath = path.join(absolutePath, dataName);
        var buffer = new Buffer(new Uint8Array(thisData));
        fs.writeFileSync(filepath, buffer, {});
        //write file end


      }
      return ret;
    },
    uploadFileImage: function(data) {
      var ret = "ok";

      //write file begin
      var data = data.replace(/^data:image\/\w+;base64,/, "");
      var fs = Npm.require('fs');
      var path = Npm.require('path');
      rootPath = path.resolve('.');
      absolutePath = rootPath.split(path.sep + '.meteor')[0] + '/public/.upload';
      var dataS = path.join(absolutePath, 'a.jpeg');
      var buffer = new Buffer(data, 'base64');
      fs.writeFile(dataS, buffer ,function(err) {
        if(err) console.log("err");
      });
      //write file end

      return dataS;
    }
  });
}
