import './profil_upload.html';

Template.profilUpload.onCreated(function() {
  this.positionLeftCanvans = new ReactiveVar(0);
  this.positionTopCanvans = new ReactiveVar(0);
  this.positionLeftImg = new ReactiveVar(0);
  this.positionTopImg = new ReactiveVar(0);
  this.widthImage = new ReactiveVar(0);
  this.heightImage = new ReactiveVar(0);
  this.imageSrc = new ReactiveVar("");
  this.count = new ReactiveVar(0);
  this.size = new ReactiveVar(0);
  this.percentage = new ReactiveVar(0);
  this.etat = new ReactiveVar(false);
});

Template.profilUpload.onRendered(function() {
  $("#file-image").hide();
});
Template.profilUpload.helpers({
  count: function() {
    return Template.instance().count.get();
  }
});
var cx = 0, cy = 0,ix = 0, iy = 0,ix2 = 0, iy2= 0;
var test = false;
Template.profilUpload.events({
  "click #btn-select-image": function(e, tpl) {
    e.stopPropagation();
    $('#file-image').trigger('click', function(e) {
      e.stopPropagation();
    });
  },
  'change #file-image': function(e, tpl) {
    var fileInput = tpl.find('#file-image');
    var za = $('#this-image');
    var canvas = tpl.find("#canvas-img");
    var zoomp = $('#zoompercentage');
    var ctx = canvas.getContext('2d');
    var fileList = fileInput.files;

    zoomp.css("left",'0px');

    var zi = $('#zoom-image');

    if(fileList[0]) {
      var tmppath = fileList[0] ? (window.URL || window.webkitURL).createObjectURL(fileList[0]) : false;
      var imgW = 0;
      var imgH = 0;
      //zi.css("left","205px");
      za.removeAttr("width");
      za.removeAttr("height");
      za.attr('src', tmppath);
      tpl.imageSrc.set(tmppath);
    }

    za.load(function(e) {
      e.preventDefault();
      ix=0;
      iy=0;
      ix2=0;
      iy2=0;
      var img = new Image();
      img.src = tmppath;
      imgW = this.width;
      imgH = this.height;
      za.addClass("myClass");
      za.attr("style", "");
      za.css("position", "absolute");
      if(imgW > imgH) {
        tpl.etat.set(false);
        this.height = 260;
        tpl.size.set(260);
        var position = (600 - this.width) / 2;
        ix=position;
        iy=30;
        za.css("left", position + "px");
        za.css("top", "30px");
        ctx.clearRect(0, 0, 240, 260);
        ctx.drawImage(img, position - 180, 0, this.width, this.height);
        tpl.positionLeftCanvans.set(position - 180);
        tpl.positionTopCanvans.set(0);
        tpl.heightImage.set(this.height);
        tpl.widthImage.set(this.width);
        tpl.positionLeftImg.set(position);
        tpl.positionTopImg.set(30);
      }
      else {
        tpl.etat.set(true);
        this.width = 240;
        tpl.size.set(240);
        var position = (320 - this.height) / 2;
        ix=180;
        iy=position;
        za.css("left", "180px");
        za.css("top", position + "px");
        ctx.clearRect(0, 0, 240, 260);
        ctx.drawImage(img, 0, position - 30, this.width, this.height);
        tpl.positionLeftCanvans.set(0);
        tpl.positionTopCanvans.set(position - 30);
        tpl.heightImage.set(this.height);
        tpl.widthImage.set(this.width);
        tpl.positionLeftImg.set(180);
        tpl.positionTopImg.set(position);
      }
    });

  },
  "mousedown #zoompercentage": function(e, tpl) {
    var za = $('#this-image');
    var iszoom = $('#iszoom');
    var zoomp = $('#zoompercentage');
    var x_ini = e.clientX;
    var y_ini = e.clientY;
    var lf = parseInt(zoomp.css("left"));
    var tp = parseInt(zoomp.css("top"));
    var ilf = parseInt(za.css("left"));
    var itp = parseInt(za.css("top"));

    iszoom.bind('mousemove', function(e) {
      var nlf = lf + e.clientX - x_ini;
      if(nlf < 0) nlf = 0;
      if(nlf > 198) nlf = 198;
      zoomp.css("left", nlf + 'px');
      zoomp.css("top", tp + 'px');

      var percentage = parseInt(nlf/2);
      var ratio = parseInt(tpl.size.get()/100);
      var size = tpl.size.get();

      za.removeAttr("width");
      za.removeAttr("height");

      if(tpl.etat.get()){
        za[0].width = size+(percentage*ratio);
      }else{
        za[0].height = size+(percentage*ratio);
      }
      ix=tpl.positionLeftImg.get()- parseInt((za[0].width - tpl.widthImage.get()) / 2);
      iy=tpl.positionTopImg.get()- parseInt((za[0].height - tpl.heightImage.get()) / 2);
      if(ix > 180) ix = 180;
      var ixt = ix +  za[0].width;
      if(ixt < 420) ix = 420-za[0].width;
      if(iy > 30) iy = 30;
      var iyt = iy + za[0].height;
      if(iyt < 290) iy = 290 - za[0].height;

      za.css("left",ix + 'px');
      za.css("top",iy + 'px');

      var canvas = tpl.find("#canvas-img");
      var ctx = canvas.getContext('2d');
      var img = new Image();
      img.src = tpl.imageSrc.get();

      ctx.clearRect(0, 0, 240, 260);
      if(tpl.etat.get()) {
        ctx.drawImage(img, parseInt(za.css("left"))-180, parseInt(za.css("top"))-30, za[0].width, za[0].height);
      }
      else{
        ctx.drawImage(img, parseInt(za.css("left"))-180, parseInt(za.css("top"))-30, za[0].width, za[0].height);
      }
    });
  },
  "mouseup #zoompercentage": function(e, tpl) {
    e.stopPropagation();
    var za = $('#this-image');
    var iszoom = $('#iszoom');
    iszoom.unbind('mousemove');
    tpl.widthImage.set(za[0].width);
    tpl.heightImage.set(za[0].height);

    tpl.positionLeftImg.set(ix);
    tpl.positionTopImg.set(iy);
  },
  "mousedown #canvas-img": function(e, tpl) {
    var canv = $("#canvas-img");
    var za = $('#this-image');
    var x_ini = e.clientX;
    var y_ini = e.clientY;
    canv.css("cursor", "all-scroll");
    var lf = parseInt(za.css("left"));
    var tp = parseInt(za.css("top"));
    var clf = parseInt(za.css("left"));
    var ctp = parseInt(za.css("top"));
    canv.bind('mousemove', function(e) {
      var za = $('#this-image');
      ix=lf + e.clientX - x_ini;
      iy=tp + e.clientY - y_ini;
      if(ix > 180) ix = 180;
      var ixt = ix +  za[0].width;
      if(ixt < 420) ix = 420-za[0].width;
      if(iy > 30) iy = 30;
      var iyt = iy + za[0].height;
      if(iyt < 290) iy = 290 - za[0].height;
      za.css("left", ix + 'px');
      za.css("top", iy + 'px');

      var canvas = tpl.find("#canvas-img");
      var ctx = canvas.getContext('2d');
      var img = new Image();
      img.src = tpl.imageSrc.get();
      ctx.clearRect(0, 0, 240, 260);

      if(tpl.etat.get()) {
        ctx.drawImage(img, parseInt(za.css("left"))-180, parseInt(za.css("top"))-30, za[0].width, za[0].height);
      }
      else{
        ctx.drawImage(img, parseInt(za.css("left"))-180, parseInt(za.css("top"))-30, za[0].width, za[0].height);
      }
    });
  },
  "mouseup #canvas-img": function(e, tpl) {
    var canv = $("#canvas-img");
    canv.css("cursor", "default");
    canv.unbind('mousemove');
    tpl.positionLeftImg.set(ix);
    tpl.positionTopImg.set(iy);
  },
  "mousemove #simg": function(e, tpl) {
    e.stopPropagation();
    var za = $('#this-image');
    var canv = $("#canvas-img");
    var iszoom = $('#iszoom');
    iszoom.unbind('mousemove');
    canv.css("cursor", "default");
    canv.unbind('mousemove');

    tpl.widthImage.set(za[0].width);
    tpl.heightImage.set(za[0].height);
    tpl.positionLeftImg.set(ix);
    tpl.positionTopImg.set(iy);
  },
  "mousemove #szoom": function(e, tpl) {
    e.stopPropagation();
    var za = $('#this-image');
    var canv = $("#canvas-img");
    var iszoom = $('#iszoom');
    iszoom.unbind('mousemove');
    canv.css("cursor", "default");
    canv.unbind('mousemove');

    tpl.widthImage.set(za[0].width);
    tpl.heightImage.set(za[0].height);
    tpl.positionLeftImg.set(ix);
    tpl.positionTopImg.set(iy);
  },
  "click #btn-save-image": function(e, tpl) {
    var canvas = tpl.find("#canvas-img");
    var data = canvas.toDataURL("image/jpeg", 1.0);
    Meteor.call("uploadFileImage", data, function(err, ret) {
      if(err) console.log('err');
      if(ret) {
        var za = $('#profil-photo');
        za.attr('src', ret);
        console.log(ret);
      }
    });
  },
});

