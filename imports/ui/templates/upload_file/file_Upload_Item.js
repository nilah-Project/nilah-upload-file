import './file_Upload_Item.html';

Template.FileUploadItem.helpers({
  isSize: function() {
    if(this.File.size < (1024 * 1024 * 1024 * 1024) && this.File.size >= (1024 * 1024 * 1024)) {
      ret = (this.File.size() / (1024 * 1024 * 1024)).toFixed(2) + ' Go';
      return ret;
    }
    if(this.File.size < (1024 * 1024 * 1024) && this.File.size >= (1024 * 1024)) {
      ret = (this.File.size / (1024 * 1024)).toFixed(2) + ' Mo';
      return ret;
    }
    if(this.File.size < (1024 * 1024) && this.File.size >= 1024) {
      ret = (this.File.size / 1024).toFixed(2) + ' ko';
      return ret;
    }
    if(this.File.size < 1024) {
      ret = this.File.size + ' o';
      return ret;
    }
  },
  isName: function() {
    if(this.File.name.length > 20) {
      var ret = this.File.name.substring(0, 17);
      ret = ret + '...';
      return ret;
    }
    return this.File.name;
  }
  ,
  isType: function() {
    if(this.File.type == "") return "Unknown";
    return this.File.type;
  },
  statusInProgress: function() {
    return (this.status == 'In Progress') ? true : false;
  },
  statusIsDone: function() {
    return (this.status == 'Done') ? true : false;
  },
});