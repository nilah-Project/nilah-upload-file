import './form_Upload.html';
import './form_Upload.css';
import './file_Upload_Item.js';

var listeFilesNews = [];
Template.FormUpload.onCreated(function() {
  this.percentage = new ReactiveVar(0);
  this.name_this_file = new ReactiveVar("file name");
  this.Listfiles = new ReactiveVar([]);
  this.ratio = new ReactiveVar(-1 / 2);
  this.clearList = new ReactiveVar(false);
  this.stopClick = new ReactiveVar(false);
});
Template.FormUpload.onRendered(function() {

  $("#select-files").hide();
  var tpl = Template.instance();
  var canvas = tpl.find("#za");
  var canvas2 = tpl.find("#az");
  var ctx = canvas.getContext('2d');
  var ctx2 = canvas2.getContext('2d');
  ctx.beginPath();
  ctx.arc(100, 100, 85, 0, 2 * Math.PI);
  ctx.lineWidth = 20;
  ctx.strokeStyle = "#fff";
  ctx.shadowOffsetX = 1;
  ctx.shadowBlur = 5;
  ctx.shadowColor = "rgba(0,0,0,0.5)";
  ctx.stroke();

  ctx2.beginPath();
  ctx2.arc(100, 100, 85, (-1 / 2) * Math.PI, tpl.ratio.get() * Math.PI);
  ctx2.lineWidth = 20;
  ctx2.strokeStyle = "#91c2ff";
  ctx2.stroke();

});
Template.FormUpload.helpers({
  Listfiles: function() {
    return Template.instance().Listfiles.get();
  },
  percentage: function() {
    return Template.instance().percentage.get();
  },
  name_this_file: function() {
    return Template.instance().name_this_file.get();
  }
});
Template.FormUpload.events({
  "click #btn-select-files": function(e, tpl) {
    e.stopPropagation();
    var selectFiles = tpl.find('#select-files');
    $('#select-files').trigger('click', function(e) {
      e.stopPropagation();
    });
  },
  'change #select-files': function(e, tpl) {
    var fileInput = tpl.find('#select-files');
    var fileList = fileInput.files;
    for(var j = 0; j < fileList.length; j++) {
      var temp = false;
      listeFilesNews.forEach(function(val) {
        if(val.File.name == fileList[j].name && val.File.size == fileList[j].size && val.File.type == fileList[j].type) {
          (temp = true);
        }
      });
      if(temp == false) {
        listeFilesNews.push({status: 'waiting', File: fileList[j]});
      }
    }
    tpl.Listfiles.set($.makeArray(listeFilesNews));
  },
  'click #clear-list-files': function(e, tpl) {
    e.stopPropagation();
    var fileInput = tpl.find('#select-files');
    var Listfiles = tpl.Listfiles.get();
    var ret = false;
    var index = -1, temp1 = 0;
    listeFilesNews.forEach(function(val) {
      if(val.status == "In Progress") {
        index = temp1;
        ret = val;
      }
      temp1++;
    });
    fileInput.value = "";
    listeFilesNews = [];
    if(index != -1) {
      listeFilesNews.push(ret);
      tpl.clearList.set(true);
      tpl.Listfiles.set($.makeArray(ret));
    } else {
      tpl.Listfiles.set([]);
    }

  },
  'click #upload-stop': function(e, tpl) {
    e.stopPropagation();
    tpl.stopClick.set(true);
  },
  'click #upload-play': function(e, tpl) {
    e.stopPropagation();
    tpl.stopClick.set(false);
    canvasProgress(tpl, 0);
    uploadItemFile(tpl, 1024 * 200, function(err, ret) {
      if(err) console.log('err');
      if(ret == 'firstItem') return 'callbackUploadItemFile';
      if(ret) {
      }
    });
  },
});

//function uploadItemFile each items file
var callbackUploadItemFile;
var uploadItemFile = function(tpl, tauxTrasfet, callback) {
  callback = callback || function() {
    };
  if(callback(null, 'firstItem') == 'callbackUploadItemFile') callbackUploadItemFile = callback;
  var Listfiles = tpl.Listfiles.get();
  var index = -1, temp1 = 0;
  Listfiles.forEach(function(val) {
    if(val.status == "waiting") {
      index = temp1;
      return
    }
    temp1++;
  });
  if(index == -1) {
    callbackUploadItemFile(null, true);
    return
  }
  if(Listfiles[index]) Listfiles[index].status = 'In Progress';
  tpl.Listfiles.set($.makeArray(Listfiles));

  tpl.name_this_file.set(Listfiles[index].File.name);
  BinaryFileReader.read(Listfiles[index].File, function(err, fileInfo) {
    isfile = fileInfo;
    data = isfile.file;
    sendData(tpl, 0, data, 0, tauxTrasfet, isfile.name, function(err, ret) {
      if(err) {
        callbackUploadItemFile(err, false);
      }
      ;
      if(ret == 'firstItem') return 'callbackSendData';
      else {
        if(!tpl.stopClick.get()) {
          tpl.percentage.set('0');
          canvasProgress(tpl, 0);
          if(!tpl.clearList.get()) listeFilesNews[index].status = 'Done';
          else {
            index = 0;
            listeFilesNews[index].status = 'Done';
            tpl.clearList.set(false);
          }
          tpl.Listfiles.set($.makeArray(listeFilesNews));
          uploadItemFile(tpl, tauxTrasfet);
        }
        if(ret == 'stop') {
          if(listeFilesNews.length == 1) index = 0;
          listeFilesNews[index].status = 'waiting';
          tpl.Listfiles.set($.makeArray(listeFilesNews));
          tpl.percentage.set('0');
          canvasProgress(tpl, 0);
          tpl.stopClick.set(false);
          return
        }
      }
    });
  });
}

//function BinaryFileReader
var BinaryFileReader = {
  read: function(file, callback) {
    var reader = new FileReader;
    var fileInfo = {
      name: file.name,
      type: file.type,
      size: file.size,
      file: null
    };
    reader.onload = function() {
      fileInfo.file = new Uint8Array(reader.result);
      callback(null, fileInfo);
    };
    reader.onerror = function() {
      callback(reader.error);
    };
    reader.readAsArrayBuffer(file);
  }
};

//function send data to server +' octe'
var callbackSendData;
var sendData = function(tpl, dataId, data, position, countData, dataName, callback) {
  callback = callback || function() {
    };
  var dataSend = new Array;
  var limit = ((position + countData) > data.length) ? data.length : position + countData;

  for(var z = position; z < limit; z++) {
    dataSend.push(data[z]);
  }

  if(callback(null, 'firstItem') == 'callbackSendData') callbackSendData = callback;
  if(tpl.stopClick.get()) {
    return callbackSendData(null, 'stop');
  }

  Meteor.call("uploadFile", dataId, data.length, dataSend, dataName, function(err, ret) {
    if(err) callbackSendData(err, false);
    if(ret) {
      if(ret.length >= data.length) {
        tpl.percentage.set('100');
        canvasProgress(tpl, 100);
        callbackSendData(null, true);
      }
      else {
        tpl.percentage.set(parseInt((ret.length / data.length) * 100));
        canvasProgress(tpl, parseInt((ret.length / data.length) * 100));
        sendData(tpl, ret._id, data, ret.length, countData, dataName);
      }
    }
  });
};
var canvasProgress = function(tpl, percentage) {
  tpl.ratio.set(percentage);
  var angle = (-1 / 2) + ((tpl.ratio.get() / 100) * 2);
  var canvas2 = tpl.find("#az");
  var ctx2 = canvas2.getContext('2d');
  ctx2.clearRect(0, 0, 200, 200);
  ctx2.beginPath();
  ctx2.arc(100, 100, 85, -1 / 2 * Math.PI, angle * Math.PI);
  ctx2.lineWidth = 20;
  ctx2.strokeStyle = "#91c2ff";
  ctx2.stroke();
}
